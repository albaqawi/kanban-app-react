import React from 'react';
// add this to ensure UI is independect from logic
import Note from './Note';

export default ({notes, onDelete=() => {}}) => (
  <ul>{notes.map(({id, task}) =>
    <li key={id}>
      <Note
        onDelete={onDelete.bind(null, id)}
        //onDelete={() => this.onDelete(id)}
        task={task} />
    </li>
  )}</ul>
)
